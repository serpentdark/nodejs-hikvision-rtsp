const Stream = require('./node-rtsp-stream');

const cameraOption = {
  user: 'admin',
  pass: 'abcd1234',
  ip: '192.168.1.64',
  port: '554',
  channel: '1'
}

const streamUrl = 'rtsp://' + cameraOption.user + ':' + cameraOption.pass + '@' + cameraOption.ip + ':' + cameraOption.port + '/Streaming/channels/' + cameraOption.channel;

stream = new Stream({
  name: 'foscam_stream',
  streamUrl: streamUrl,
  wsPort: 9999,
  width: 1280,
  height: 720
});
