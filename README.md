# NodeJS Hikvision RTSP

Web Streaming ดึงวิดีโอจากกล้อง Hikvision มาแสดงบนเว็บโดยใช้ NodeJS + RTSP + FFMPEG

## Maintainer
[Wuttichai Prasertsang](www.facebook.com/serpentdark)

## ตั้งค่ากล้องก่อนการติดตั้ง
เข้าไปที่ ip กล้องบนเว็บ browser แล้วตั้งค่าดังนี้

```
- Configuration -> System -> Security -> Authentication เลือก digest/basic ทั้ง RTSP Authentication และ WEB Authentication กด Save
```
```
- Configuration -> Network -> Advanced Setting -> Integration Protocol เลือก Enable Hikvision-CGI แล้วเลือก digest/basic กด Save
```
```
- Configuration -> Video/Audio -> Video ตรง Resolution ให้เปลี่ยนเป็น 1280*720P กด Save
```

## ติดตั้ง
1. [NodeJS >= 8](https://nodejs.org/en/)
2. [FFMPEG](https://ffmpeg.zeranoe.com/builds/) Download Build

## ติดตั้ง Source Code

```
git clone https://gitlab.com/serpentdark/nodejs-hikvision-rtsp.git
cd nodejs-hikvision-rtsp
// ตั้งค่า cameraOption ในไฟล์ index.js
npm install
npm start
// เปิดไฟล์ test_client.html บน browser
```

## Please Enjoy!!!
